#!/usr/bin/env bash
# Run MediaWiki jobs
#
# SPDX-License-Identifier: GPL-2.0-or-later
set -euo pipefail

cd $HOME/public_html/w/

while true; do
    # Run all notification jobs. Should be "cheap"
    php maintenance/run.php runJobs --type=enotifNotify

    # Wait for up to 20 jobs to be ready to run and run them
    php maintenance/run.php runJobs --wait --maxjobs=20

    # Take a little nap to be nice to other things running on the same cpu
    sleep 60
done
