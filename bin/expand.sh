#!/usr/bin/env bash
# Replace %%...%% references with envvars.
#
# SPDX-License-Identifier: GPL-2.0-or-later
set -euo pipefail

regex='%%([a-zA-Z_][a-zA-Z_0-9]*)%%'
while read line; do
    while [[ "$line" =~ $regex ]]; do
        param="${BASH_REMATCH[1]}"
        line=${line//${BASH_REMATCH[0]}/${!param:?${param} not found in env}}
    done
    echo $line
done
